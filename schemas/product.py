from datetime import datetime
from uuid import uuid4
from pydantic import BaseModel, Field


def generate_date():
    return str(datetime.now())


def generate_id():
    return str(uuid4())


class Product(BaseModel):
    id: str = Field(default_factory=generate_id)
    name: str
    price: float
    date: str = Field(default_factory=generate_date)
