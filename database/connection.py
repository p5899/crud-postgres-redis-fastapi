from sqlalchemy import create_engine, MetaData
from os import getenv


PASSWORD = getenv("POSTGRES_PASSWORD")
PORT = getenv("POSTGRES_PORT")
DBNAME = getenv("POSTGRES_DBNAME")

postgres_url = f"postgresql://postgres:{PASSWORD}@localhost:{PORT}/{DBNAME}"

engine = create_engine(postgres_url)

meta = MetaData()

conn = engine.connect()
