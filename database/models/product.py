from sqlalchemy import Column, Table
from sqlalchemy.sql.sqltypes import String, Float
from database.connection import meta, engine

products = Table(
    "products",
    meta,
    Column("id", String, primary_key=True),
    Column("name", String(255)),
    Column("price", Float),
    Column("date", String),
)

meta.create_all(engine)
