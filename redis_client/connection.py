from redis import Redis
from redis.exceptions import ConnectionError
from os import getenv

try:
    redisClient = Redis(
        host=getenv("REDIS_HOST"),
        port=getenv("REDIS_PORT"),
        password=getenv("REDIS_PASSWORD"),
        ssl=False)
    print("CONNECTED TO REDIS!")
except ConnectionError as e:
    print(e)
