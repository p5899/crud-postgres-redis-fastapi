from .connection import redisClient
from redis.exceptions import ResponseError


def save_hash(key: str, data: dict):
    try:
        redisClient.hset(key, mapping=data)
    except ResponseError as e:
        print(e)


def get_hash(key: str):
    try:
        return redisClient.hgetall(name=key)
    except ResponseError as e:
        print(e)


def delete_hash(key: str, keys):
    try:
        redisClient.hdel(key, *keys)
    except ResponseError as e:
        print(e)
