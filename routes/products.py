from fastapi import APIRouter
from schemas.product import Product

# DATABASE
from database.models.product import products
from database.connection import conn

# REDIS
from redis_client.crud import delete_hash, get_hash, save_hash

routes_products = APIRouter()


@routes_products.post("/create", response_model=Product)
def create(product: Product):
    try:
        # SQLALCHEMY
        conn.execute(products.insert().values(product.dict()))

        # REDIS
        save_hash(key=product.dict()["id"], data=product.dict())

        return product
    except Exception as e:
        return e


@routes_products.get("/get/{id}")
def create(id: str):
    try:
        # REDIS GET
        data = get_hash(key=id)

        if len(data) == 0:
            # SQLALCHEMY
            product = conn.execute(
                products.select().where(products.c.id == id)).first()
            # REDIS SAVE
            save_hash(key=product["id"], data=product)

            return product
        else:
            return data
    except Exception as e:
        return e


@routes_products.delete("/remove/{id}")
def create(id: str):
    try:
        keys = Product.__fields__.keys()

        # SQLALCHEMY
        conn.execute(products.delete().where(products.c.id == id))

        # REDIS
        delete_hash(key=id, keys=keys)
        return "success"
    except Exception as e:
        return e
