from fastapi import FastAPI
from routes.products import routes_products


app = FastAPI()

app.include_router(routes_products, prefix="/product")
